
#SPAMTON G SPAMTON DISCORD BOT INTENDED ONLY FOR r/THEKINDERGUY DISCORD SERVER
#COPYRIGHT HARUEXPLAINER 2003 PATENT PENDING
#TODO: ASK SOMEONE FOR SPELLING MISTAKES THERE'S PROBABLY A LOT OF THEM

#dialogflow agent link: https://dialogflow.cloud.google.com/#/agent/spamton-proyect-vwj


#Discord Imports
import discord
from discord.ext import commands
#Dialogflow Imports
import os
import dialogflow_v2
from google.api_core.exceptions import InvalidArgument

#Credentials
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "spamton-proyect-vwvj-6380b66871e9.json"

DIALOGFLOW_PROJECT_ID = "spamton-proyect-vwvj"
DIALOGFLOW_LANGUAGE_CODE = "en"
SESSION_ID = "me"


#Discord client bootup
client = commands.Bot(command_prefix = "HeySpamton" , intents = discord.Intents.all())  

#In-code variables
p_m = False
# puppet mode toggle
rate = 10000#k
# x% of soul = rate * x


#Auction file reading (exits auction dictionary)
file = open("AuctionCurrent.txt","r")
auction = {}
for line in file:
    line=line.strip().split(":")
    auction[line[0]]=line[1]
file.close()

#User files reading (exits userData dictionary)
file = open("users.txt","r")  
userData = {}
for username in file:
    username=username.strip()
    try:
        userFile = open(username+".txt","r")
    except:
        userFile = open(username+".txt","w")
        userFile.write("Krommer:0\nSoul:100\n")
        userFile.close()
        userFile = open(username+".txt","r")
    finally:
        userData[username]={}
        for line in userFile:
            line = line.strip().split(":")
            if line[0]=="Krommer" or line[0]=="Soul":
                userData[username][line[0]]=float(line[1])
            else:
                userData[username][line[0]]=line[1]
        
        userFile.close()

#On_ready print (can add other bootup info here)
@client.event
async def on_ready():
    print("\nI'm online under the name: {0.user}\n".format(client))



#Main code (run on every message sent)
@client.event
async def on_message(message):

    #Message logging

    mensaje=message.content.split(" ")

    if message.author == client.user:
        return

    message.author = str(message.author)

    print("User",message.author,"says:")
    print(message.content,'\n')

    if mensaje[0]!="HeySpamton":
        return


    #Response protocol

    #Manual
    if p_m == True:
        await message.channel.send(input("Respond: "))
        return
    
    #Automatic
    else:

        #Auction command {HeySpamton auction}
        if mensaje[1]== "auction":
            await message.channel.send("THE CURRENT PRODUCT ON SALE IS [["+auction["Item"]+ "]] FROM THE [["+auction["Universe"]+ 
            "]] UNIVERSE!\nTHE LAST BID WAS MADE BY USER [["+auction["LastBidder"]+ "]], WHO BID [["+auction["Currentbid"]+ 
            "]] DELICIOUS KROMMER.\n YOU'VE GOT TILL [["+auction["Ends"]+"]] TO BID A HIGHER AMOUNT AND HAVE [[moral superiority]].\n"+
            " REMEMBER TO USE [[HeySpamton bid <amount>]] TO BID A HIGHER AMOUNT!!!!")
            return

        #Bid command {HeySpamton bid int<amount>}
        elif mensaje[1]=="bid":
            try:
                amount = int(mensaje[2])

            except:
                await message.channel.send("SORRY PAL, SOMETHING MUST'VE GONE WRONG BECAUSE THAT'S NOT A [[valid amount]] OF KROMMER TO BID")
                return

            else:
                if message.author not in userData:
                    await message.channel.send("HI USER [["+message.author+"]], IT APPEARS THIS IS YOUR FIRST TIME TRYING TO BID."+
                    " I'LL BREAK THE NEWS TO YOU AS QUICK AS I CAN, YOU'RE [[dead broke]] AND CURRENTLY HAVE 0 KROMMER."+
                    " IF YOU WISH TO BID ON AN AUCTION, YOU MUST SELL PART OF YOUR [[heartshaped object]] USING [[HeySpamton sell_soul <percent>]]"+
                    " TO SELL PART OF YOUR SOUL TO ME IN EXCHANGE FOR DELICIOUS KROMMER!!")

                    userData[message.author]={"Krommer":0,"Soul":100}
                    userFile = open(message.author+".txt","w")
                    userFile.write("Krommer:0\nSoul:100\n")
                    userFile.close()
                    return


                elif userData[message.author]["Krommer"]<=0:
                    await message.channel.send("HELLO USER [["+message.author+"]], I HAVE BAD NEWS FOR YOU PAL. YOU APPEAR TO HAVE RUN OUT OF KROMMER!!! IF YOU WANT MORE "+
                    "[[succulent]] KROMMER, YOU NEED ONLY SELL ME PART OF YOUR [[heartshaped object]], USING [[HeySpamton sell_soul <percent>]].")
                    return

                elif amount <= int(auction["Currentbid"]):
                    await message.channel.send("HELLO USER [["+message.author+"]], I HAVE BAD NEWS FOR YOU PAL. I DON'T KNOW IF YOU ACTUALLY READ"+
                    " THE CURRENT AUCTION BUT THIS AMOUNT IS [[lower]] THAN THE AMOUNT ALREADY BID BY [["+auction["LastBidder"]+"]]."+
                    " TRY AGAIN WHEN YOU'VE GOT A LITTEL MORE KROMMER")
                    return

                else:
                    await message.channel.send("AN EXCELLENT CHOICE!!! I WILL RAISE THE BID RIGHT NOW TO [["+amount+
                    "]]. BY THE WAY USER [["+message.author+"]] DON'T TELL ANYONE BUT I'M BETTING ON YOU WINNING THIS AUCTION.")

                    auction["Currentbid"]=amount
                    auction["LastBidder"]=message.author

                    file = open("AuctionCurrent.txt","w")
                    for key in auction:
                        file.write(key+":"+auction[key])
                    file.close()
                    return

        #Sell soul command {HeySpamton sell_soul float<percent>} ; percent e [0,100[
        elif mensaje[1]=="sell_soul":

            try:
                percent = float(mensaje[2])
                if percent>100 or percent<=0:
                    raise
                gain_krommer = percent*rate
            
            except:
                await message.channel.send("SORRY PAL, SOMETHING MUST'VE GONE WRONG BECAUSE THAT'S NOT A [[valid amount]] OF SOUL TO TRADE FOR KROMMER")
                return

            else:
                if message.author not in userData:
                    userData[message.author]={"Krommer":gain_krommer,"Soul":(100-percent)}
                    userFile = open(message.author+".txt","w")
                    userFile.write("Krommer:"+str(gain_krommer)+"\nSoul:"+str(100-percent)+"\n")
                    userFile.close()
                    await message.channel.send("CONGRATULATIONS USER [["+message.author+
                    "]] YOU ARE NOW THE PROUD OWNER OF EXACTLY [["+str(userData[message.author]["Krommer"])+
                    "]] SCRUMPTIOUS KROMMER. DON'T SPEND IT ALL IN ONE PLACE!!!")
                    return

                elif float(userData[message.author]["Soul"])<percent:
                    await message.channel.send("WOAH THERE [[user]], IT LOOKS LIKE YOU'RE TRYING TO SELL MORE SOUL THAN YOU'VE GOT."+
                    " MAYBE BE MORE CAREFULL WITH WHAT OTHER [[lovecraftian man made horrors]] YOU ARE TRADING YOUR SOUL WITH")

                    return

                else:
                    userData[message.author]["Krommer"]+=gain_krommer
                    userData[message.author]["Soul"]-=percent

                    userFile = open(message.author+".txt","w")
                    for key in userData[message.author]:
                        userFile.write(key+":"+userData[message.author][key])
                    userFile.close()

                    await message.channel.send("CONGRATULATIONS USER [["+message.author+
                    "]] YOU ARE NOW THE PROUD OWNER OF EXACTLY [["+userData[message.author]["Krommer"]+
                    "]] SCRUMPTIOUS KROMMER. DON'T SPEND IT ALL IN ONE PLACE!!!")
                    return

        #Normal conversation {HeySpamton str<message>}
        else:

            text_to_be_analyzed = message.content[11:]

            session_client = dialogflow_v2.SessionsClient()
            session = session_client.session_path(DIALOGFLOW_PROJECT_ID, SESSION_ID)
            text_input = dialogflow_v2.types.TextInput(text=text_to_be_analyzed, language_code=DIALOGFLOW_LANGUAGE_CODE)
            query_input = dialogflow_v2.types.QueryInput(text=text_input)
            
            try:
                response = session_client.detect_intent(session=session, query_input=query_input)
            except InvalidArgument:
                await message.channel.send("I COULDN'T [[catch]] THAT ONE CHIEF COULD YOU PLEASE [[give another shot]]")
            else:
                await message.channel.send(response.query_result.fulfillment_text)


password = "Insert password here"
#Client ex (Keep pasword secret)
client.run(password)
